Чтобы получить путь конфигурационных файлов, используемых PostgreSQL, необходимо выполнить соответствующий запрос.

##### Для получения пути к `postgresql.conf` ([[Конфигурация PostgreSQL]]):
```SQL
SHOW config_file;
```

##### Для получения пути к `pg_hba.conf` ([[Настройка доступов PostgreSQL]]):
```SQL
SHOW hba_file;
```


*NB: Для дальшейшего манипулирования можно использовать следующие варианты*

Выполнение команды с выводом пути:
 ```bash
 psql -t -P format=unaligned -c '<COMMAND>'
 ```

Открытие файла в `VIM` ([[Текстовый редактор Vim]]):
```bash
vim $(psql -t -P format=unaligned -c '<COMMAND>')
```



Теги: #postgres #administration #DevOps #database