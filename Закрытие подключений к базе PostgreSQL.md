*Для выполнения некоторых команд требуется, чтобы БД была свободна от подключений.*

В PostgreSQL подключение можно убить с помощью функции `pg_terminate_backend`, которая в качестве параметра принимает `pid` подключения. `pid` может быть получен из системного отношения `pg_stat_activity` ([[Системные отношения PostgreSQL]], [[Активные подключения к PostgreSQL]]).

Для того чтобы закрыть подключения к определённой БД, можно выполнить запрос при помощи `psql` ([[Утилита PSQL]]):

```SQL
SELECT pg_terminate_backend(pg_stat_activity.pid)
FROM pg_stat_activity
WHERE pg_stat_activity.datname = '<TARGET_DB>';
```

**`TARGET_DB` не может быть БД, из которой выполняется запрос на закрытие.**

*NB: Может понадобиться применить несколько раз, из-за образования новых подключений.*



Теги: #postgres #administration #DevOps #database