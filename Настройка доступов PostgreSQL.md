*Ограничение доступа необходимо, для того чтобы предотвратить консолидацию персональных данных пользователей/клиентов и защитить данные от несанкционированного изменения.*

В файле `pg_hba.conf` содержатся записи, определяющие какие адреса могут подключаться к серверу бд.

### Структура записи
Запись представляет собой строку формата:

```
host          database  user  address     auth-method  [auth-options]
```

Или:

```
host          database  user  IP-address  IP-mask      auth-method  [auth-options]
```

Или для локального подключения:

```
local         database  user  auth-method [auth-options]
```

_Также можно определить записи отдельно на безопасные соединения._

-   `hostssl`
-   `hostnossl`
-   `hostgssenc`
-   `hostnogssenc`

### Методы аутентификации
-   `trust`
-   `reject`
-   `scram-sha-256`
-   `md5`
-   `password`
-   `gss`
-   `sspi`
-   `ident`
-   `peer`
-   `ldap`
-   `radius`
-   `cert`
-   `pam`
-   `bsd`

**Для того чтобы изменения вступили в силу необходимо выполнить перезагрузку конфигов ([[Перезагрузка конфигов PostgreSQL]])**



Теги: #postgres #administration #security #DevOps #database