#### С помощью `psql` ([[Утилита PSQL]])
```sql
SELECT pg_reload_conf();
```

#### С помощью `systemctl` ([[Утилита systemctl]])
```bash
systemctl restart postgres
```



Теги: #postgres #administration #DevOps #database